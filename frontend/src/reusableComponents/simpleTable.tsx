import React, { BaseSyntheticEvent, useEffect, useState } from "react";
import { TableProps } from "./../interfaces/table";


export default function SimpleTable(props: TableProps) {
    let { data } = props;
    let tableCells: Array<any> = [
        <span key="bookmark_header">Bookmark</span>,
        <span key="id_header">Id</span>,
        <span key="forks_header">Forks Count</span>,
        <span key="watch_header">Watch Count</span>,
        <span key="repo_header">Repository URL</span>
    ];

    const [isBookmarked, setIsBookmarked] = useState<any>({});

    useEffect(() => {
        setIsBookmarked(props.areBookmarked);
    }, [props.areBookmarked]);

    const toggleBookmark = (e: BaseSyntheticEvent, rowId: any) => {
        setIsBookmarked((prev: any) => ({...prev , [rowId]: e.target.checked}));

        if (e.target.checked) {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
            };
            fetch(`/api/bookmarks/${rowId}`, requestOptions);
        } else {
            const requestOptions = {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json' },
            };
            fetch(`/api/bookmarks/${rowId}`, requestOptions);
        }
    }

    for (const repo of data) {
        tableCells.push(
            <span key={repo.id}><input type="checkbox" checked={isBookmarked[repo.id] !== undefined ? isBookmarked[repo.id] : false} onChange={(e) => toggleBookmark(e, repo.id)}/></span>
        );
        for (const prop in repo) {
            if (repo.hasOwnProperty(prop)) {
                tableCells.push(
                    <span key={`${repo.id}_value`}>{repo[prop]}</span>
                )
            }
        }
        
    }
    return (
        <div className="table-container">
            {tableCells}
        </div>
    );
};