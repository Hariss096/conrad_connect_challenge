import React, { BaseSyntheticEvent, useState } from 'react';
import './App.css';
import SimpleTable from './reusableComponents/simpleTable';

function App() {
  const [searchText, setSearchText] = useState<string>("");

  const [responseData, setResponseData] = useState<Array<{}>>([]);
  const [areBookmarked, setAreBookmarked] = useState({});
  const handleInputChange = (e: BaseSyntheticEvent) => {
    setSearchText(e.target.value);
  };

  const fetchGithubRepos = async () => {
    const data = await fetch(`/api?search=${encodeURIComponent(searchText)}`)
    .then((response) => {
        return response.json();
      })
    .catch(err => console.log(err));
    setResponseData(data);
  };

  const fetchBookmarkedRepos = async () => {
    const data = await fetch(`/api/bookmarks`).then((response) => {
      return response.json();
    }).catch(er => console.log(er));

    let bookmarks = {};
    data.map((repo: any) => (bookmarks = {...bookmarks, [repo.id]: true}));
    setAreBookmarked(bookmarks);
    setResponseData(data);
  }

  return (
    <div className="App">
      <div className="nav">
        <button onClick={fetchBookmarkedRepos}>Bookmarked Repositories</button>
      </div>
      <input onChange={handleInputChange} className="seach-input"/>
      <button onClick={fetchGithubRepos} className="btn">Search</button>
      <SimpleTable data={responseData} areBookmarked={areBookmarked}/>
    </div>
  );
}

export default App;
