import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Hello World Text', () => {
  render(<App />);
  const basicText = screen.getByText(/Hello World/i);
  expect(basicText).toBeInTheDocument();
});
