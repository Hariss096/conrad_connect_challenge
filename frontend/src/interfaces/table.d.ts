export interface ResponseData {
    [key: string]: number | string,
}

export interface TableProps {
    data: Array<ResponseData>;
    areBookmarked: {
        [key: string]: boolean,
    };
}