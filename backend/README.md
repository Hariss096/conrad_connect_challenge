A simple backend for searching and bookmarking github repositories from their public API.


Github API docs can be found: [here](https://docs.github.com/en/rest)

Before starting the server for the first time, please install npm dependencies using `npm install`.

You will have to create a `.env` file inside /backend and populate values given in `.env.example`.

For starting the server:
1. `cd backend`
2. `npm start`

When the server is started for the first time, a build folder will be created inside /backend which contains the minimized backend code (also ready for production).