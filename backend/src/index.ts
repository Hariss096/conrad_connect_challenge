import express from "express";
import fetch from "node-fetch";
import dotenv from "dotenv";
import { Logger } from "ts-node-logger";
import { RepositorySearchResponse, SearchApiResponse } from "./interfaces/search";
import BookmarkRoutes from "./routes/bookmarks";

dotenv.config();

const logger = Logger.getLogger();

const app = express();

app.get( "/api", async ( req, res ) => {
    if (req.query.search) {
        const queryString = "q=" + encodeURIComponent(req.query.search as string);
        const response = await fetch(`https://api.github.com/search/repositories?${queryString}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/vnd.github.v3+json",
                "Authorization": `token ${process.env.GH_ACCESS_TOKEN}`
            },
        });
        const responseData: RepositorySearchResponse = await response.json() as RepositorySearchResponse;
        const filteredResponseData: SearchApiResponse[] = responseData.items.map(
            ({id, forks_count, watchers_count, url}) => ({id, forks_count, watchers_count, url}));
        res.header("Content-Type", "application/json");
        res.send( JSON.stringify(filteredResponseData) );
    } else {
        res.send( "" );
    }
} );

app.use("/api/bookmarks", BookmarkRoutes);

app.get( "/", ( req, res ) => {
    res.send( "Hi!" );
} );

app.listen( process.env.PORT, () => {
    logger.info( `server running at http://localhost:${ process.env.PORT || 5000 }` );
} );
