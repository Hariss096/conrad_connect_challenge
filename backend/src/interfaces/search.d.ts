export interface SearchApiResponse {
    id: number;
    forks_count: number;
    watchers_count: number;
    url: string;
}


export interface RepositorySearchResponse {
    incomplete_results: boolean;
    items: Array<SearchApiResponse>;
    total_count: number;
}
