import express from "express";
import fetch from "node-fetch";
import fs from "fs";    // For mock DB
import https from "https"

const app = express();

app.post( "/:repoId", async ( req, res ) => {
    let bookmarks: any[] = [];
    try {
        const savedBookmarks = fs.readFileSync('bookmarks.json', 'utf8');
        bookmarks = JSON.parse(savedBookmarks);
    } catch (err) {
        console.error(err);
    }

    bookmarks = [...bookmarks, req.params.repoId];
    try {
        fs.writeFileSync("bookmarks.json", JSON.stringify(bookmarks));
        console.log("Bookmarks updated");
        return res.send(req.params.repoId);
    }
    catch (err) {
        if (err) throw err;
    }
    });


app.get( "/", async ( req, res ) => {
    try {
        let savedBookmarks = fs.readFileSync('bookmarks.json', 'utf8');
        savedBookmarks = JSON.parse(savedBookmarks);
        let responses: Array<any> = [];

        for (const savedRepoId of savedBookmarks) {
            const response = await fetch(`https://api.github.com/repositories/${savedRepoId}`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/vnd.github.v3+json",
                    "Authorization": `token ${process.env.GH_ACCESS_TOKEN}`
                },
            });

            try {
                let responseObject: any = await response.json();
                let {id, forks_count, watchers_count, url} = responseObject;
                let filteredResponseData = {id, forks_count, watchers_count, url};
                responses.push(filteredResponseData);
            } catch(err) {
                console.error(err);
            }
        }
    
        res.header("Content-Type", "application/json");
        res.send( responses );
    } catch (err) {
        console.error(err);
    }
    });

app.delete( "/:repoId", async ( req, res ) => {
    let bookmarks: any[] = [];
    try {
        const savedBookmarks = fs.readFileSync('bookmarks.json', 'utf8');
        bookmarks = JSON.parse(savedBookmarks);
    } catch (err) {
        console.error(err);
    }

    bookmarks = bookmarks.filter(bookmark => bookmark != req.params.repoId);
    try {
        fs.writeFileSync("bookmarks.json", JSON.stringify(bookmarks));
        console.log(`Bookmark with id ${req.params.repoId} deleted`);
        return res.send(req.params.repoId);
    }
    catch (err) {
        if (err) throw err;
    }
    });

export default app;