## Conrad Connect Coding Challenge

This application contains frontend for UI/UX that will be rendered in the browser and a backend that contains all the server side logic.


The frontend is build with react, styling is currently done with plain CSS. The docs are here : [Frontend_README](frontend/README.md)

The backend is build with express (Typescript) on top of Node. The docs are here : [Backend_README](backend/README.md)